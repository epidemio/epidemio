## Install

```bash
pip install -r requirements.txt
```

## Render with [voila](https://voila.readthedocs.io/en/stable/index.html)

```bash
voila --template=material SEIR.ipynb
```

## Launch with [Binder](https://mybinder.org/)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fepidemio%2Fepidemio/master?urlpath=voila)

